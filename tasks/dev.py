from invoke import task


@task(default=True)
def dev(context):
    """
    Start dev server
    """
    from orama import start_server
    server = start_server()
    return server.run()
