import unittest
from orama import start_server

class Test(unittest.TestCase):

    def setUp(self):
        self.server = start_server().test_client()

    def test_success(self):
        results = self.server.post('/challenge', json={
            'freelance': {
                'id': 42,
                'professionalExperiences': [
                {
                    'id': 4,
                    'startDate': '2016-01-01T00:00:00+01:00',
                    'endDate': '2018-05-01T00:00:00+01:00',
                    'skills': [
                    {
                        'id': 241,
                        'name': 'React'
                    },
                    {
                        'id': 270,
                        'name': 'Node.js'
                    },
                    {
                        'id': 370,
                        'name': 'Javascript'
                    }
                    ]
                }
                ]
            }
        })

        self.assertEqual(results.status_code, 200)
        self.assertEqual(results.json, {
            'freelance': {
                'id': 42,
                'computedSkills': [
                    {
                        'id': 241,
                        'name': 'React',
                        'durationInMonths': 28
                    },
                    {
                        'id': 270,
                        'name': 'Node.js',
                        'durationInMonths': 28
                    },
                    {
                        'id': 370,
                        'name': 'Javascript',
                        'durationInMonths': 28
                    }
                ]
            }
        })


    def test_unprocessable(self):
        results = self.server.post('/challenge', json={'freelance': {}})
        self.assertEqual(results.status_code, 422)
        self.assertEqual(results.json, {'error': 'freelance: id: Missing data for required field.'})
