import os
from flask import request
from flask_restplus import Resource, Namespace, fields, marshal_with
from .schemas import FreelancerSchema
from .business import sum_freelancer_hours
from orama.core.errorhandler import ErrorHandler

ns = Namespace("challenge", url_prefix="/", description="Sum the hours of a freelancer")

@ns.route("challenge")
class Challenge(Resource):
    def post(self):
        try:
            payload = FreelancerSchema().load(request.json)
            results = sum_freelancer_hours(payload)
            return results, 200

        except Exception as error:
            return ErrorHandler(error).handle()
